require 'debci/amqp'
require 'debci/config'

module Debci
  class Backend
    # Select which backend to use for the given package. The backend specified
    # in the package `backend` attribute will be used if that backend is
    # available for the given architecture, or otherwise the default backend
    # will be used as a fallback.
    def self.select(package, arch)
      if package.backend && Debci::AMQP.queue_exists?(arch, package.backend)
        package.backend
      else
        Debci.config.backend
      end
    end
  end
end
